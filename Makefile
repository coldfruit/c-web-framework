LD := clang
LDFLAGS := -pthread
CFLAGS := -Wall -Wextra -std=gnu11 -g
OBJS := main.o

all: www

www: $(OBJS)
	$(LD) $(LDFLAGS) -o www $(OBJS)

clean:
	rm -f www
	rm -f $(OBJS)