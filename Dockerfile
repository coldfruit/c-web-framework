FROM debian:stable as build

RUN apt-get update
RUN apt-get install -y make libsqlite3-dev clang
RUN update-alternatives --set cc /usr/bin/clang

COPY . /src
WORKDIR /src

RUN make

FROM debian:stable as run

COPY --from=build /src/www /app/www

ENTRYPOINT [ "/app/www" ]