#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <wchar.h>
#include <signal.h>
#include <arpa/inet.h>
#include "sqlite3.h"

// 512K receive buffer "ought to be enough for anyone"
#define RECV_BUFLEN (512 * 1024)
#define SEND_BUFLEN (512 * 1024)

#define HEADER_NAME_MAX 8192
#define HEADER_VALUE_MAX 8192

// maximum number of headers we support - bear in mind request memory usage will get quite high if we increase this
// as we don't dynamically resize the headers array (yet).
#define MAX_HEADERS 100

// max URL size
#define MAX_URL 1024

enum log_level {
    DEBUG,
    WARNING,
    ERROR
};

typedef enum log_level log_level;

void write_log(log_level level, const char *format, ...);

// set to 0 to gracefully shutdown
int running = 1;

enum http_method {
    GET,
    POST,
    PATCH,
    PUT,
    DELETE,
    OPTIONS
};

enum status_code {
    // informational
    CONTINUE = 100,
    SWITCHING_PROTOCOLS = 101,
    PROCESSING = 102,
    EARLY_HINTS = 103,

    // success
    OK = 200,
    CREATED = 201,
    ACCEPTED = 202,
    // forget 203, it sucks
    NO_CONTENT = 204,

    // redirect
    FOUND = 301,

    // client error
    BAD_REQUEST = 400,
    FORBIDDEN = 401,

    UNAUTHORIZED = 403,
    NOT_FOUND = 404,

    // server error
    INTERNAL_SERVER_ERROR = 500
};

typedef enum status_code status_code;
typedef enum http_method http_method;

struct http_header {
    char name[HEADER_NAME_MAX];
    // header values can be UTF-8 encoded or even any arbitrary binary... thanks W3C...
    char value[HEADER_VALUE_MAX];
};

typedef struct http_header http_header;

struct http_request {
    http_method method;
    char url[MAX_URL];
    unsigned int header_count;
    http_header *headers;
    char *body;
};

struct http_response {
    status_code code;
    http_header *headers;
    char *body;
    int header_count;
};

typedef struct http_request http_request;
typedef struct http_response http_response;

status_code parse_request(unsigned char *raw, http_request *request) {
    memset(request, 0, sizeof *request);
    char *line = NULL;
    line = strtok((char *) raw, "\n");

    // if there is no first line in the request, the request is by definition malformed (0-length requests are bad... obviously)
    if (line == NULL) {
        return BAD_REQUEST;
    }

    // no verbs are less than 3 characters long, so again by definition this is an invalid request
    if (strlen(line) < 3) {
        return BAD_REQUEST;
    }

    switch (line[0]) {
        case 'G':
            // GET
            if (strncmp(line, "GET", 3) != 0) {
                // invalid method = bad request
                return BAD_REQUEST;
            }
            request->method = GET;
            strncpy(request->url, line + 4, MAX_URL);
            break;
        case 'P':
            // either PUT, POST, or PATCH
            switch (line[1]) {
                case 'U':
                    // PUT
                    if (strncmp(line, "PUT", 3) != 0) {
                        // invalid method = bad request
                        return BAD_REQUEST;
                    }
                    request->method = PUT;
                    strncpy(request->url, line + 4, MAX_URL);
                    break;
                case 'O':
                    // POST
                    if (strncmp(line, "POST", 4) != 0) {
                        return BAD_REQUEST;
                    }
                    request->method = POST;
                    strncpy(request->url, line + 5, MAX_URL);
                    break;
                case 'A':
                    // PATCH
                    if (strncmp(line, "PATCH", 5) != 0) {
                        return BAD_REQUEST;
                    }
                    request->method = PATCH;
                    strncpy(request->url, line + 6, MAX_URL);
                    break;
            }
            break;
        case 'D':
            // DELETE
            if (strncmp(line, "DELETE", 6) != 0) {
                return BAD_REQUEST;
            }
            request->method = DELETE;
            strncpy(request->url, line + 7, MAX_URL);
            break;
        case 'O':
            // OPTIONS
            if (strncmp(line, "OPTIONS", 7) != 0) {
                return BAD_REQUEST;
            }
            request->method = OPTIONS;
            strncpy(request->url, line + 8, MAX_URL);
            break;
    }

    int parse_body = 0;
    unsigned long body_read = 0;
    // default content length = 1MB - client must provide content length to get more
    unsigned int content_length = 1024 * 1024;
    // todo: dynamically resize headers array as needed
    request->headers = calloc(MAX_HEADERS, sizeof(*request->headers));

    // move onto the next line (first line is method, URL and version [which we ignore])
    line = strtok(NULL, "\n");

    while (line != NULL) {
        // blank new line means end of headers
        if (line[0] == '\n') {
            parse_body = 1;
            request->body = malloc(content_length);
        }

        if (parse_body) {
            if (body_read + strlen(line) <= content_length) {
                memcpy((request->body + body_read), line, strlen(line));
                body_read += strlen(line);
            }
        } else {
            if (request->header_count < MAX_HEADERS) {
                char c = line[0];
                unsigned int i = 0;
                // find index of ':'
                while (c != ':' && i < strlen(line)) {
                    c = line[++i];
                }
                // cap header name
                if (i > HEADER_NAME_MAX) {
                    i = HEADER_NAME_MAX;
                }
                printf("%s\n", (line + i));
                memcpy(request->headers[request->header_count].name, line, i);
                //                                                                      +2 to skip the ": " sequence
                memcpy(request->headers[request->header_count].value, line + i + 2, strlen(line) - i);
                request->header_count++;
            }
        }

        line = strtok(NULL, "\n");
    }
    return OK;
}

const char *method_tostr(http_method method) {
    switch (method) {
        case GET:
            return "GET";
        case PUT:
            return "PUT";
        case PATCH:
            return "PATCH";
        case POST:
            return "POST";
        case OPTIONS:
            return "OPTIONS";
        case DELETE:
            return "delete";
        default:
            return "invalid method";
    }
}

void print_request(http_request request) {
    printf("Method: %s\n", method_tostr(request.method));
    printf("URL: %s\n", request.url);
    printf("Headers:\n");
    unsigned int header_num = 0;
    while (header_num < request.header_count) {
        http_header header = request.headers[header_num];
        printf("\t%s: %s\n", header.name, header.value);
        header_num++;
    }
    printf("Body:\n%s", request.body);
}

char *code2str(status_code code) {
    char *rv = "???";

    switch (code) {
        case CONTINUE:
            rv = "CONTINUE";
            break;
        case SWITCHING_PROTOCOLS:
            break;
        case PROCESSING:
            break;
        case EARLY_HINTS:
            break;
        case OK:
            rv = "OK";
            break;
        case CREATED:
            break;
        case ACCEPTED:
            break;
        case NO_CONTENT:
            break;
        case FOUND:
            break;
        case BAD_REQUEST:
            break;
        case FORBIDDEN:
            break;
        case UNAUTHORIZED:
            break;
        case NOT_FOUND:
            break;
        case INTERNAL_SERVER_ERROR:
            break;
    }
    return rv;
}

void serialize_response(unsigned char *dest, http_response response) {
    char* dest_as_char = (char*) dest;
    sprintf(dest_as_char, "HTTP/1.1 %d %s\n", response.code, code2str(response.code));
    for (int i = 0; i < response.header_count; i++)
    {
        http_header header = response.headers[i];
        sprintf(dest_as_char + strlen(dest_as_char), "%s: %s\n", header.name, header.value);
    }
    sprintf(dest_as_char + strlen(dest_as_char), "\n%s\n", response.body);
}

http_header make_header(const char *name, const char *value) {
    http_header result;
    memcpy(result.name, name, strlen(name));
    memcpy(result.value, value, strlen(value));
    return result;
}

int handle_connection(int clientfd) {
    unsigned char *receive_buffer = malloc(RECV_BUFLEN);
    http_response response;
    http_request request;

    memset(&response, 0, sizeof response);

    // do one receive
    if (recv(clientfd, receive_buffer, RECV_BUFLEN, 0) > 0) {
        // parse receive_buffer into a http request object
        response.code = parse_request(receive_buffer, &request);

        print_request(request);

        // clean up request and response objects
        free(request.body);

        // clear receive buffer for next recv call
        memset(receive_buffer, 0, RECV_BUFLEN);
    }
    // send back the response
    write_log(DEBUG, "Request finished parsing");

    unsigned char *send_buffer = malloc(SEND_BUFLEN);

    // todo: pass request and address of response out to a plugin, for now we just always respond the same thing
    response.header_count = 2;
    response.headers = calloc(response.header_count, sizeof *response.headers);
    response.headers[0] = make_header("Content-Length", "3");
    response.headers[1] = make_header("Server", "I'm in your server header, h4xxing");
    response.body = "OK";

    write_log(DEBUG, "Finished processing request, responding");

    serialize_response(send_buffer, response);

    write_log(DEBUG, "Response serialized as:\n%s\n", send_buffer);

    send(clientfd, send_buffer, strlen((char *) send_buffer), 0);

    // close the connection (todo: HTTP2 support lol)
    close(clientfd);

    // assuming all went well, return a nice lovely zero for this thread
    return 0;
}

static void *worker_thread(void *arg) {
    int clientfd = *(int *) arg;
    int *worker_thread_exit_status = malloc(sizeof *worker_thread_exit_status);

    pthread_detach(pthread_self());

    printf("Worker thread started to handle connection on socket %d\n", clientfd);

    *worker_thread_exit_status = handle_connection(clientfd);

    printf("Worker thread exiting as connection on socket %d was closed\n", clientfd);

    free(arg);

    return worker_thread_exit_status;
}

const char *log_level2str(log_level level) {
    switch (level) {
        case DEBUG:
            return "DEBUG";
        case WARNING:
            return "WARNING";
        case ERROR:
            return "ERROR";
        default:
            return "?????";
    }
}

// todo: configurable logging - log formats, log backends, etc.
void write_log(log_level level, const char *format, ...) {
    printf("[%s] ", log_level2str(level));
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    // ensure last character in the log entry is always a newline character
    if (format[strlen(format) - 1] != '\n') {
        putchar('\n');
    }
    va_end(args);
}

void handle_signal(int signal) {
    switch (signal) {
        case SIGTERM:
            running = 0;
            break;
        case SIGKILL:
        case SIGSTOP:
            write_log(DEBUG,
                      "System allowed catching sigkill/sigstop, this is more than likely an error in your operating system.  Please submit a bug report upstream!");
            running = 0;
            break;
        default:
            write_log(DEBUG, "Unknown signal caught (programming error)");
            break;
    }
}

int main(void) {
    // will bring these back when we start building in db support
    // sqlite3* db = NULL;
    // sqlite3_stmt *statement = NULL;
    struct addrinfo hints, *res;
    int sockfd = 0;
    struct sockaddr_storage client_addr;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    char *bind_address = getenv("PORT");
    if (bind_address == NULL)
    {
        bind_address = "9980";
    }

    if (getaddrinfo(NULL, bind_address, &hints, &res) != 0) {
        // todo: maybe route these errors via write_log instead?
        perror("Could not get address information");
        exit(-1);
    }

    // hack to listen on `::` instead of `0.0.0.0` (allowing both IPv4 AND IPv6 connections)
    if (res->ai_next != NULL) {
        res = res->ai_next;
    }

    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sockfd == -1) {
        perror("Could not create socket");
        exit(-1);
    }

    if (bind(sockfd, res->ai_addr, res->ai_addrlen) != 0) {
        perror("Could not bind to address");
        exit(-1);
    };

    // stolen from nginx's NGX_LISTEN_BACKLOG macro
    listen(sockfd, 511);

    write_log(DEBUG, "Listening on %s", bind_address);

    // discarded after use, but required as arg to pthread_create
    pthread_t thread_id = 0;

    // these two shouldn't actually work (SIGKILL and SIGSTOP are "uncatchable" signals)
    signal(SIGKILL, handle_signal);
    signal(SIGSTOP, handle_signal);
    // the only really important one
    signal(SIGTERM, handle_signal);

    while (running) {
        unsigned int sin_size = sizeof client_addr;
        int *clientfd = malloc(sizeof *clientfd);
        *clientfd = accept(sockfd, (struct sockaddr *) &client_addr, &sin_size);
        if (*clientfd == -1) {
            perror("Error accepting connection");
            continue;
        }
        char client_addr_str[INET6_ADDRSTRLEN > INET_ADDRSTRLEN ? INET6_ADDRSTRLEN : INET_ADDRSTRLEN]
                = {'\0'};

        unsigned int port = 0;

        switch (client_addr.ss_family) {
            case AF_INET: {
                struct sockaddr_in *addr_in = (struct sockaddr_in *) &client_addr;
                inet_ntop(AF_INET, &(addr_in->sin_addr), client_addr_str, INET_ADDRSTRLEN);
                port = addr_in->sin_port;
                break;
            }
            case AF_INET6: {
                struct sockaddr_in6 *addr_in6 = (struct sockaddr_in6 *) &client_addr;
                inet_ntop(AF_INET6, &(addr_in6->sin6_addr), client_addr_str, INET6_ADDRSTRLEN);
                port = addr_in6->sin6_port;
            }
        }
        write_log(DEBUG, "Got connection from %s:%d", client_addr_str, port);
        // in case we want to use non-null attributes later, leave this here
        // but do not pass in an empty 'attr' struct!  pthread will try to allocate 0 bytes of stack
        // and you will scratch your head for an hour about why you're getting ENOMEM errors
        pthread_attr_t *attr = NULL;
        int thread_err = pthread_create(&thread_id, attr, &worker_thread, clientfd);
        if (thread_err != 0) {
            perror("Could not create thread");
        }
    }

    close(sockfd);
    return 0;
}
